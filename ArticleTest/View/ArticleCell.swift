//
//  ArticleModel.swift
//  Article Search
//
//  Created by Tariq Najib on 23/08/2019.
//  Copyright © 2019 Tariq Najib. All rights reserved.
//
import UIKit

class ArticleCell: UITableViewCell {
    
    var articleViewModel: ArticleViewModel! {
        didSet {
            textLabel?.text = articleViewModel.headline
            detailTextLabel?.text = articleViewModel.dateTextString
            accessoryType = articleViewModel.accessoryType
        }
    }
    
    override func setHighlighted(_ highlighted: Bool, animated: Bool) {
        super.setHighlighted(highlighted, animated: animated)
        contentView.backgroundColor = isHighlighted ? .highlightColor : .white
        textLabel?.textColor = isHighlighted ? UIColor.white : .mainTextBlue
        detailTextLabel?.textColor = isHighlighted ? .white : .black
    }
    
    override init(style: UITableViewCellStyle, reuseIdentifier: String?) {
        super.init(style: .subtitle, reuseIdentifier: reuseIdentifier)
        
        // cell customization
        textLabel?.font = UIFont.boldSystemFont(ofSize: 24)
        textLabel?.numberOfLines = 0
        detailTextLabel?.textColor = .black
        detailTextLabel?.font = UIFont.systemFont(ofSize: 20, weight: .light)
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError()
    }
}
