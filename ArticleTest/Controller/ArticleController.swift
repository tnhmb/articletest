//
//  ArticleModel.swift
//  Article Search
//
//  Created by Tariq Najib on 23/08/2019.
//  Copyright © 2019 Tariq Najib. All rights reserved.
//
import UIKit

enum SEARCH_TYPE {
    case email
    case share
    case view
    case query
}
class ArticleController: UITableViewController {
    
    var page = 1
    var isGetResponse = true
    var searchType: SEARCH_TYPE?
    var articleViewModels = [ArticleViewModel]()
    let cellId = "cellId"
    var searchQuery = ""
    override func viewDidLoad() {
        super.viewDidLoad()

        setupNavBar()
        setupTableView()
        self.articleViewModels.removeAll()
        self.tableView.reloadData()
        fetchDataWithSearch()
    }
    
    func fetchDataWithSearch() {
        
        if searchType == SEARCH_TYPE.query {
            Service.shared.fetchArticlesByValue(page: "\(page)", query: searchQuery ,completion: { (topLevel, err) in
                if let err = err {
                    print("Failed to fetch courses:", err)
                    return
                }
                for (val) in (topLevel?.response.docs)! {
                    let articleViewModel = ArticleViewModel(article: val)
                    self.articleViewModels.append(articleViewModel)
                }
                
                self.tableView.reloadData()
            })
        } else if searchType == SEARCH_TYPE.email {
            Service.shared.fetchArticlesByMostEmailed(page: "\(1)", completion: { (topLevel, err) in
                if let err = err {
                    print("Failed to fetch courses:", err)
                    return
                }
                for (val) in (topLevel!.results) {
                    let articleViewModel = ArticleViewModel(article: val)
                    self.articleViewModels.append(articleViewModel)
                }
                
                self.tableView.reloadData()
            })
        } else if searchType == SEARCH_TYPE.view {
            Service.shared.fetchArticlesByMostViewed(page: "\(1)", completion: { (topLevel, err) in
                if let err = err {
                    print("Failed to fetch courses:", err)
                    return
                }
                for (val) in (topLevel?.results)! {
                    let articleViewModel = ArticleViewModel(article: val)
                    self.articleViewModels.append(articleViewModel)
                }
                
                self.tableView.reloadData()
            })
        } else if searchType == SEARCH_TYPE.share {
            Service.shared.fetchArticlesByMostShared(page: "\(1)", completion: { (topLevel, err) in
                if let err = err {
                    print("Failed to fetch courses:", err)
                    return
                }
                for (val) in (topLevel?.results)! {
                    let articleViewModel = ArticleViewModel(article: val)
                    self.articleViewModels.append(articleViewModel)
                }
                
                self.tableView.reloadData()
            })
        }
        
    }
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return articleViewModels.count
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: cellId, for: indexPath) as! ArticleCell
        let articleViewModel = articleViewModels[indexPath.row]
        cell.articleViewModel = articleViewModel
        
        return cell
    }
    override func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        if indexPath.row == self.articleViewModels.count - 3 && isGetResponse {

            page = page + 1
            print("page \(page)")
            fetchDataWithSearch()
            if page == 100 {
                isGetResponse = false
                page = 1
            }
        }
    }
    func setupTableView() {
        tableView.register(ArticleCell.self, forCellReuseIdentifier: cellId)
        tableView.separatorInset = UIEdgeInsets(top: 0, left: 24, bottom: 0, right: 24)
        tableView.separatorColor = .mainTextBlue
        tableView.backgroundColor = UIColor.rgb(r: 12, g: 47, b: 57)
        tableView.rowHeight = UITableViewAutomaticDimension
        tableView.estimatedRowHeight = 50
        tableView.tableFooterView = UIView()
    }
    
    func setupNavBar() {
        navigationItem.title = "Articles"
        navigationController?.navigationBar.prefersLargeTitles = true
        navigationController?.navigationBar.backgroundColor = .yellow
        navigationController?.navigationBar.isTranslucent = false
        navigationController?.navigationBar.barTintColor = UIColor.rgb(r: 50, g: 199, b: 242)
        navigationController?.navigationBar.largeTitleTextAttributes = [.foregroundColor: UIColor.white]
    }
    
}

class CustomNavigationController: UINavigationController {
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .lightContent
    }
}

extension UIColor {
    static let mainTextBlue = UIColor.rgb(r: 7, g: 71, b: 89)
    static let highlightColor = UIColor.rgb(r: 50, g: 199, b: 242)
    
    static func rgb(r: CGFloat, g: CGFloat, b: CGFloat) -> UIColor {
        return UIColor(red: r/255, green: g/255, blue: b/255, alpha: 1)
    }
}
