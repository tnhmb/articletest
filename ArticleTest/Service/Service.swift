//
//  Service.swift
//  MVC
//
//  Created by Brian Voong on 6/30/18.
//  Copyright © 2018 Brian Voong. All rights reserved.
//

import Foundation

class Service: NSObject {
    static let shared = Service()
    static let apikey = "Z85hmbkqr6Vs3esA73zz1AKOzMe4UYGJ"
    func fetchArticlesByValue(page: String, query: String, completion: @escaping (TopLevel?, Error?) -> ()) {
        let urlString = "https://api.nytimes.com/svc/search/v2/articlesearch.json?q=\(query)&api-key=\(Service.apikey)&page=\(page)"
        guard let url = URL(string: urlString) else { return }
        URLSession.shared.dataTask(with: url) { (data, resp, err) in
            if let err = err {
                completion(nil, err)
                print("Failed to fetch articles:", err)
                return
            }
            
            // check response
            
            guard let data = data else { return }
            do {
                let articles = try JSONDecoder().decode(TopLevel.self, from: data)
                DispatchQueue.main.async {
                    completion(articles, nil)
                }
            } catch let jsonErr {
                print("Failed to decode:", jsonErr)
            }
            }.resume()
    }
    
    func fetchArticlesByMostEmailed(page: String, completion: @escaping (Root?, Error?) -> ()) {
        let urlString = "https://api.nytimes.com/svc/mostpopular/v2/emailed/7.json?api-key=\(Service.apikey)&page=\(page)"
        guard let url = URL(string: urlString) else { return }
        URLSession.shared.dataTask(with: url) { (data, resp, err) in
            if let err = err {
                completion(nil, err)
                print("Failed to fetch articles:", err)
                return
            }
            
            // check response
            
            guard let data = data else { return }
            do {
                let articles = try JSONDecoder().decode(Root.self, from: data)
                DispatchQueue.main.async {
                    completion(articles, nil)
                }
            } catch let jsonErr {
                print("Failed to decode:", jsonErr)
            }
            }.resume()
    }
    
    func fetchArticlesByMostViewed(page: String, completion: @escaping (Root?, Error?) -> ()) {
        let urlString = "https://api.nytimes.com/svc/mostpopular/v2/viewed/1.json?api-key=\(Service.apikey)&page=\(page)"
        guard let url = URL(string: urlString) else { return }
        URLSession.shared.dataTask(with: url) { (data, resp, err) in
            if let err = err {
                completion(nil, err)
                print("Failed to fetch articles:", err)
                return
            }
            
            // check response
            
            guard let data = data else { return }
            do {
                let articles = try JSONDecoder().decode(Root.self, from: data)
                DispatchQueue.main.async {
                    completion(articles, nil)
                }
            } catch let jsonErr {
                print("Failed to decode:", jsonErr)
            }
            }.resume()
    }
    
    func fetchArticlesByMostShared(page: String, completion: @escaping (Root?, Error?) -> ()) {
        let urlString = "https://api.nytimes.com/svc/mostpopular/v2/shared/1/facebook.json?api-key=\(Service.apikey)&page=\(page)"
        guard let url = URL(string: urlString) else { return }
        URLSession.shared.dataTask(with: url) { (data, resp, err) in
            if let err = err {
                completion(nil, err)
                print("Failed to fetch articles:", err)
                return
            }
            
            // check response
            
            guard let data = data else { return }
            do {
                let articles = try JSONDecoder().decode(Root.self, from: data)
                DispatchQueue.main.async {
                    completion(articles, nil)
                }
            } catch let jsonErr {
                print("Failed to decode:", jsonErr)
            }
            }.resume()
    }
}
