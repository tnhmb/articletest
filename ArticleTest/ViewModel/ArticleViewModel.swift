//
//  ArticleModel.swift
//  Article Search
//
//  Created by Tariq Najib on 23/08/2019.
//  Copyright © 2019 Tariq Najib. All rights reserved.
//

import Foundation
import UIKit

struct ArticleViewModel {
    
    var headline: String
    
    var dateTextString: String
    var accessoryType: UITableViewCellAccessoryType
    
    // Dependency Injection (DI)
    init(article: Article) {
        self.headline = article.headline.main ?? article.lead_paragraph
        let format = "yyyy-MM-dd'T'HH:mm:ssZ"
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = format
        dateFormatter.timeZone = TimeZone(abbreviation: "GMT")
        let dateString = dateFormatter.date(from: article.pub_date)
        
        let dateFormatterPrint = DateFormatter()
        dateFormatterPrint.dateFormat = "d MMM yy h:mm a"
        dateFormatterPrint.timeZone = TimeZone.current
        dateTextString = dateFormatterPrint.string(from: dateString ??  Date(timeIntervalSinceReferenceDate: -123432.0))
        //dateTextString = "\(dateString)"
        accessoryType = .none
        
    }
    init(article: ArticleFiltered) {
        self.headline = article.title
        dateTextString = article.published_date
        accessoryType = .none
    }
    
}

