//
//  ArticleModel.swift
//  Article Search
//
//  Created by Tariq Najib on 23/08/2019.
//  Copyright © 2019 Tariq Najib. All rights reserved.
//

import Foundation





struct Article: Decodable {
    let web_url: String
    let snippet: String
    let lead_paragraph: String
    let abstract: String
    let headline: Headline
    let pub_date: String
}
struct Response:Decodable {
    let docs: [Article]
}
struct TopLevel : Decodable {
    let status : String
    let copyright: String
    let response : Response
}
struct Headline: Decodable {
    let main: String?
}

