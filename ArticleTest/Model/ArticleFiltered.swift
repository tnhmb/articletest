//
//  ArticleFiltered.swift
//  ArticleTest
//
//  Created by Tariq Najib on 23/08/2019.
//  Copyright © 2019 Brian Voong. All rights reserved.
//

import Foundation
struct ArticleFiltered: Decodable {
    let abstract: String
    let title: String
    let published_date: String
}
struct Root : Decodable {
    let status : String
    let copyright: String
    let results : [ArticleFiltered]
}
