//
//  ArticleModel.swift
//  Article Search
//
//  Created by Tariq Najib on 23/08/2019.
//  Copyright © 2019 Tariq Najib. All rights reserved.
//

import UIKit

class ViewController: UIViewController {

    @IBOutlet weak var searchArea: UITextField!
    @IBOutlet weak var mostEmailedBtn: UIButton!
    @IBOutlet weak var mostViewedBtn: UIButton!
    @IBOutlet weak var mostSharedBtn: UIButton!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        buttonListeners()
        setupSearchArea()
        
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    func setupSearchArea() {
        searchArea.delegate = self
        searchArea.layer.cornerRadius = 20
        searchArea.layer.masksToBounds = true
    }
    
    func buttonListeners() {
        
        mostSharedBtn.addTarget(self, action: #selector(mostSharedClicked), for: .touchUpInside)
        
        mostEmailedBtn.addTarget(self, action: #selector(mostEmailedClicked), for: .touchUpInside)
        
        
        mostViewedBtn.addTarget(self, action: #selector(mostViewedClicked), for: .touchUpInside)
    }
    
    @objc func mostSharedClicked () {
        print("share clicked")
        let vc = ArticleController()
        vc.searchQuery = ""
        vc.isGetResponse = false
        vc.page = 1
        vc.searchType = SEARCH_TYPE.share
        self.navigationController?.pushViewController(vc, animated: true)
    }
    @objc func mostViewedClicked () {
        let vc = ArticleController()
        vc.searchQuery = ""
        vc.isGetResponse = false
        vc.page = 1
        vc.searchType = SEARCH_TYPE.view
        self.navigationController?.pushViewController(vc, animated: true)
    }
    @objc func mostEmailedClicked () {
        let vc = ArticleController()
        vc.searchQuery = ""
        vc.isGetResponse = false
        vc.page = 1
        vc.searchType = SEARCH_TYPE.email
        self.navigationController?.pushViewController(vc, animated: true)
    }
}

extension ViewController: UITextFieldDelegate {
    func textFieldDidEndEditing(_ textField: UITextField) {
        
        
    }
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        let vc = ArticleController()
        vc.searchQuery = textField.text ?? ""
        vc.searchType = SEARCH_TYPE.query
        vc.isGetResponse = true
        vc.page = 1
        self.navigationController?.pushViewController(vc, animated: true)
        return true
    }
}
