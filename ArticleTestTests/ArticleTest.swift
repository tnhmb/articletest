//
//  mvc_to_mvvm_demoTests.swift
//  mvc_to_mvvm_demoTests
//
//  Created by Brian Voong on 7/3/18.
//  Copyright © 2018 Brian Voong. All rights reserved.
//

import XCTest
@testable import ArticleTest

class ArticleTestTests: XCTestCase {
    
    override func setUp() {
        super.setUp()
    }
    
    override func tearDown() {
        super.tearDown()
    }
    
    func testArticleViewModel() {
        let article = Article(web_url: "www.wikipedia.com", snippet: "hello world", lead_paragraph: "Its a beautiful day today", abstract: "Beautiful day", headline: Headline(main: "The Day Today"), pub_date: "2019-08-23T09:19:58+0000")
        let articleViewModel = ArticleViewModel(article: article)
        
        XCTAssertEqual(article.headline.main, articleViewModel.headline)
        XCTAssertEqual("date: \(article.pub_date)", articleViewModel.dateTextString)
        XCTAssertEqual(.none, articleViewModel.accessoryType)
    }
    
    func testArticleViewModelLessonsOverThreshold() {
        let article = Article(web_url: "www.wikipedia.com", snippet: "hello world", lead_paragraph: "Its a beautiful day today", abstract: "Beautiful day", headline: Headline(main: "The Day Today"), pub_date: "2019-08-23T09:19:58+0000")
        let articleViewModel = ArticleViewModel(article: article)
        
        XCTAssertEqual("The Day Today", articleViewModel.headline)
    }
    
    
    
    
    
    
    
    
    
    
    
}
